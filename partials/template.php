<!DOCTYPE html>
<html>
<head>
	<title></title>

	<!-- bootswatch -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/journal/bootstrap.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  		<a class="navbar-brand" href="../index.php">UAPLG</a>
  		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
 		<span class="navbar-toggler-icon"></span></button>

	  	<div class="collapse navbar-collapse" id="navbarColor02">
	    	<ul class="navbar-nav mr-auto">
	      	<li class="nav-item active">
	      	<a class="nav-link" href="catalog.php">Item list<span class="sr-only">(current)</span></a>
	      	</li>
	      	<?php 
	      		session_start();
	      		if(isset($_SESSION['email']) && $_SESSION['email']=="admin@email.com"){
	      		?>
	      		<li class="nav-item">
	      			<a class="nav-link" href="add-item.php">Add item</a>
	      		</li>
	      	 <?php
	      	}
	      	else{
	      	 	?>
	     			<li class="nav-item">
	       				<a class="nav-link" href="cart.php">Cart <span class="badge bg-warning">
	       					<?php
	       					if(isset($_SESSION["cart"])){
	       						echo array_sum($_SESSION["cart"]);
	       					}
	       					else{
	       						echo 0;
	       					}
	       				?>
	       				</span></a>
	      			</li>
	      		<?php 
	      	}
	      		if(isset($_SESSION['firstName'])){
	      	?>
	      	<li class="nav-item">
	      		<a class="nav-link" href="#">Hello <?php echo $_SESSION['firstName']?></a>
	      	</li>
	      	<li class="nav-item">
	      		<a class="nav-link" href="../controllers/logout-process.php">Logout</a>
	      	</li>
	      	<?php
	      	}
	      	else{
	      	?>
	      	<li class="nav-item">
	      		<a class="nav-link" href="login.php">Login</a>
	      	</li>
	      	<li class="nav-item">
	      		<a class="nav-link" href="register.php">Register</a>
	      	</li>
	      	<?php	
	      	}	 
	     	?>
	    	</ul>
	   		 <form class="form-inline my-2 my-lg-0">
	    	 	<input class="form-control mr-sm-2" type="text" placeholder="Search">
	    	  	<button class="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
	    	</form>
	  	</div>
	</nav>

	<!-- page content -->

	<?php get_body_contents()?>


	<!-- footer -->
	<footer class="page-footer font-small bg-dark">
		<div class="footer-copyright text-center py-3 text-white">footer text copyright here</div>
	</footer>
</body>
</html>