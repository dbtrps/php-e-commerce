<?php 
	session_start();

	// remove all session variables;
	session_unset();

	// destroy the sessions;
	session_destroy();

	header("LOCATION: ../index.php");

 ?>